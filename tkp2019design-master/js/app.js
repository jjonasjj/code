// Gallery filter
$('.btn').click(function(){
  //var btnValue = $(this).text();
  var btnValue = $(this).data('filter');
  console.log(btnValue)
  let imgHolder = $('.featured-works-gallery-item-holder');
  $('.featured-works-gallery-item-holder-item').each(function(){
    if(!$(this).hasClass(''+btnValue)){
      console.log("non-match")
      $(this).each(function(){
        $(this).hide();
      })
    } else if ($(this).hasClass(''+btnValue)) {
      $(this).each(function(){
        $(this).show();
      })
    }
    if (btnValue == 'all'){
     $(this).each(function(){
       $(this).show();
     }) 
    }
      
  })
  
})

// Mobile menu

$('.mobile-navigation-button').click(function(){
  $('.nav-navigation').toggleClass("nav-navigation-mobile-active");
})

// Gallery Image Hover
$('.featured-works-gallery-item-holder-item').hover(function(){
  $(this).children('.featured-item-description').addClass("show-overlay")
  $(this).children('.featured-item-description').removeClass("hidden")}, function(){
  $(this).children('.featured-item-description').removeClass("show-overlay")
  $(this).children('.featured-item-description').addClass("hidden")
});

// Load skills

var num = 0;
function skillCounter(item, count){
  if (num <= count){
    setTimeout(function(){
      item.text(num + "%")
      num++;
      skillCounter(item, count);
    }, 20)
  }
}

var widthNum = 0;
function widthCounter(item, count) {
  if (widthNum <= count){
  setTimeout(function(){
    item.css("width", widthNum + "%");
    widthNum++;
    widthCounter(item, count);
  }, 20)
}
}

function displaySkills() {
  $('.our-skills-display-skill-bar > *').each(function(){
    var currentWidth = $(this).attr("data-width").slice(0,-1);
    var currentItem = $(this);
    widthCounter(currentItem, currentWidth);
  })

  $('.our-skills-display-skill-percentage').each(function(){
    var currentNumber = $(this).attr("data-percentage")
    var currentItem = $(this);
    skillCounter(currentItem, currentNumber);
  })
}

var waypoint = new Waypoint({
  element: $('.our-skills'),
  handler: function(){
    displaySkills();
  }
})


// Client Slider
$(document).ready(function(){
  $('.clients-holder').slick({
    infinite: false,
    arrows: true,
    nextArrow: '<i class="fas fa-arrow-circle-right"></i>',
    prevArrow: '<i class="fas fa-arrow-circle-left"></i>',
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
})

// Form checking

// Characters limited to 250
var form_comment = $('#message');
var error_holder = $('.error-holder');
console.log("TEST working")
console.log(error_holder)
form_comment.on('input', function(){
  var current_chars = form_comment[0].value.length;
    console.log(current_chars)
    if (current_chars > 250) {
      form_comment.addClass("character-limit");
      let error_msg = document.createElement('div');
      let error_txt = document.createTextNode('Too many characters: ' + current_chars); error_msg.appendChild(error_txt);
      error_holder.html(error_msg.textContent);
    } else {
      form_comment.removeClass("character-limit");
      error_holder.html('');
    }
})

var form = $('.contact-us-form-main');
var empty_holder = $(".error-holder");
var success_holder = $(".success-holder");

form.on("submit", formCheck);

function formCheck(e){
  e.preventDefault();
  var empty_field = '';
  var final_chars = form_comment[0].value.length;
  if (form[0]["firstName"].value == ''){
    empty_field = "Jusu Vardas";
  }else if (form[0]["lastName"].value == ''){
    empty_field = "Jusu Pavarde";
  }else if (form[0]["email"].value == ''){
    empty_field = "Jusu Pastas";
  }else if (form[0]["subject"].value == ''){
    empty_field = "Jusu Zinutes pavadinimas";
  }else if (form[0]["message"].value == ''){
    empty_field = "Jusu Komentaras";
  } else if (final_chars > 250) {
    empty_field = "Per daug zodziu komentare"
  } else {
  }
  console.log(empty_field)
  if (empty_field === '') {
    empty_holder.removeClass('show-message');
    success_holder.addClass('show-message');
    success_holder.html("Zinute issiusta");
  } else if (empty_field != ''){
    empty_holder.addClass('show-message');
    empty_holder.html(empty_field);
    empty_field = '';
    success_holder.removeClass('show-message');
    success_holder.html(' ');
  }
}

//Detect overflow in menu
function checkContainerSize(){
  var container = $('.secondary-nav-menu');
  console.log(container[0].scrollWidth)
}

$('.nav-navigation li').hover(function(){
  console.log("HOVERED")
  checkContainerSize();
  console.log(screen.width);
})
